class CreateInvoices < ActiveRecord::Migration[5.1]
  def change
    create_table :invoices do |t|
      t.string :invoicenumber
      t.string :companyname
      t.string :manager
      t.date :dategenerated
      t.string :paid
      t.string :amountdue
      t.text :terms
      t.string :accountmanager

      t.timestamps
    end
  end
end
